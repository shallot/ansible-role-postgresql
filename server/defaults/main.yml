# Copyright (c) 2018-2023 eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

# Main server configuration (postgresql.conf) directives
postgresql_server_configuration:
  {}

# Whether to "reload" or "restart" the service after configuration updates
# This only needs to be changed to restart if you change options in
# postgresql_server_configuration that require a restart, otherwise a
# reload will fail. Typical examples include:
#   listen_addresses: "*" # listen to remote connections, not just localhost
#   max_connections: "50" # reduce default to use fewer resources
# NB: an inclusive list of these options be obtained with e.g.
#   sudo -u postgres psql template1 -c \
#     "select name from pg_settings where context = 'postmaster'"
postgresql_service_strategy:
  "reload"

# Host-based authentication (pg_hba.conf) directives that we need to connect
# acceptable format is:
#   type + database + user + address + auth_method + auth_options
postgresql_hba_entries:
  []

# The version of PostgreSQL, used in configuration file paths below.
postgresql_server_version:
  "{{ postgresql_default_server_version }}"

# The path to the PostgreSQL configuration directory
postgresql_configuration_directory:
  "{{ postgresql_default_configuration_directory }}"

# The path to the PostgreSQL server configuration file
postgresql_server_configuration_path:
  "{{ postgresql_configuration_directory }}/postgresql.conf"

# The path to the PostgreSQL server authentication configuration
# NB: could be extracted from postgresql_server_configuration_path
# as "hba_file", but hell
postgresql_hba_path:
  "{{ postgresql_configuration_directory }}/pg_hba.conf"

# Whether to initialize a PostgreSQL instance after package installation
postgresql_initdb:
  "{{ postgresql_default_initdb | default(false) }}"

# The name of the PostgreSQL server package
postgresql_server_package_name:
  "{{ postgresql_default_package_name }}"

# Whether to enable the service to start during system boot
postgresql_service_enabled:
  true

# The actual name of the PostgreSQL server service
postgresql_service_name:
  "postgresql"

# The target state of the service; either "started" or "stopped"
postgresql_service_state:
  "{{ postgresql_service_enabled | ternary('started', 'stopped') }}"

# The name of the PostgreSQL system user
postgresql_system_user:
  "postgres"

# Package name of the Python library Ansible uses to talk to PostgreSQL
python_psycopg2_package_name:
  "{{ python_psycopg2_default_package_name }}"

# The names of databases to create
postgresql_databases:
  {}

# The names of users to create
postgresql_users:
  {}

# The position indicator (blockinfile insertafter) where the authentication
# configuration should be added (because in pg_hba.conf, order matters)
postgresql_hba_entries_placement:
  "EOF"
